import ballerina/graphql;

public type CovidStatistics record {
    string date;
    readonly string region;
    int deaths;
    int confirmed_cases;
    int recoveries;
    int tested;
};

public distinct service class CovidStatistic {
    CovidStatistics stats;

    function init(CovidStatistics stats) {
        self.stats = stats;
    }

    resource function get region() returns string {
        return self.stats.region;
    }
    resource function get date() returns string {
        return self.stats.date;
    }
    resource function get deaths() returns int {
        return self.stats.deaths;
    }
    resource function get confirmed_cases() returns int {
        return self.stats.confirmed_cases;
    }
    resource function get recoveries() returns int {
        return self.stats.recoveries;
    }
    resource function get tested() returns int {
        return self.stats.tested;
    }

}

service / on new graphql:Listener(9000) {

    table<CovidStatistics> key(region) statistics = table
[
    {date: "12/09/2021", region: "Khomas", deaths: 39, confirmed_cases: 465, recoveries: 67, tested: 1200}
];

    resource function get statistics(string region) returns CovidStatistic?|error {

        CovidStatistics? s = self.statistics[region];

        if s is () {

            return;
        }

        return new (s);
    }
    remote function update(CovidStatistics covidStatistics) returns CovidStatistic {

        CovidStatistic stat = new CovidStatistic(covidStatistics);

        if self.statistics[covidStatistics.region] is () {
            self.statistics.add(covidStatistics);

        }
        else {

            self.statistics.put(covidStatistics);
        }
        return stat;
    }
}
