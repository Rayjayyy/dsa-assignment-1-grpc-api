import ballerina/grpc;
import ballerina/io;

AssessmentManagementServerClient ep = check new ("http://localhost:9090");
AssessmentRequest[] unmarked = [];

public function main() returns error? {
    io:println("Login");

    error? dashboard = Dashboard();

}

function Dashboard() returns error? {
    string userTypeString = "";
    string type1 = io:readln("Learner or Assessor (1=Learner, 2=Assessor, 3=Administrator):  ");
    int|error type2 = int:fromString(type1);
    if (check type2 == 1) {
        userTypeString = "Learner";
    } else if (check type2 == 2) {

        userTypeString = "Assessor";

    } else if (check type2 == 3) {

        userTypeString = "Administrator";

    }
    string id1 = io:readln("Enter ID: ");
    int|error id2 = int:fromString(id1);

    RequestUser tUser = {
        usertype: userTypeString,
        user_id: check id2
    };
    io:println(tUser);

    UserRequest userRes = check ep->request_users(tUser);
    io:println(userRes);
    if (userRes.user_id == check id2) {

        if (userRes.usertype == "Assessor") {
            string Ans1 = io:readln("(1=Request Assignments, 2=Submit Marks, 3=Log out):  ");
            int|error ans1 = int:fromString(Ans1);
            if (ans1 == 1) {
                error?|string requestAssignmentsResult = requestAssignments(userRes.courses);

            }
            if (ans1 == 2) {
                error?|string submitMarksResult = submitMarks(userRes.courses);
            } else {
                error? dashboard = Dashboard();

            }

        }
        if (userRes.usertype == "Administrator") {
            string Ans2 = io:readln("(1=Create User, 2=Create Course, 3=Assign Course, 4=Log out):  ");
            int|error ans2 = int:fromString(Ans2);
            if (ans2 == 1) {
                error? user = createUser();

            }
            if (ans2 == 2) {
                error? createResult = createCourse();

            }
            if (ans2 == 3) {
                error? assignCourseResult = assignCourse();

            } else {
                error? dashboard = Dashboard();

            }
        }
        if (userRes.usertype == "Learner") {
            string Ans3 = io:readln("(1=Register Learner, 2=Submit Assignment, 3=Log out:  ");
            int|error ans3 = int:fromString(Ans3);

            if (ans3 == 1) {
                error? registerLearnerResult = registerLearner();

            }
            if (ans3 == 2) {
                error? submitAssignmentResult = submitAssignment();

            } else {
                error? dashboard = Dashboard();

            }
        }
    }
}

function requestAssignments(string[] allocatedCourse) returns error?|string {

    io:println("Allocated Courses: ", allocatedCourse);

    string assign = io:readln("Enter Course code for assignment: ");
    foreach var item in allocatedCourse {
        if (item == assign) {
            RequestAssignment req = {
                id: 0,
                message: assign
            };
            stream<AssessmentRequest, grpc:Error?> streamResult = check ep->request_assignments(req);
            check streamResult.forEach(function(AssessmentRequest i) {
                io:println("Unmarked submitted assignment:", i);
                unmarked.push(i);
                error? dashboard = Dashboard();

            });
        }
else {
            //return "Entered Course code not registered for";
            error? dashboard = Dashboard();

        }
    }

}

function submitMarks(string[] allocatedCourses) returns error? {
    AssessmentRequest[] marked = [];
    io:println(allocatedCourses);
    string cCode = io:readln("Enter Course Code: ");
    foreach var item1 in allocatedCourses {
        if (item1 == cCode) {
            foreach var item in unmarked {
                if (cCode == item.code) {
                    io:println("Learner ID: ", item.name);
                    io:println("Course Code: ", item.code);
                    io:println("Assignment Content: ", item.assigmentwork);
                    string assignMark = io:readln("Enter Mark for assignment: ");
                    int|error assignmark = int:fromString(assignMark);
                    AssessmentRequest req = {
                        code: cCode,
                        name: item.name,
                        assigmentwork: item.assigmentwork,
                        assessment_mark: check assignmark,
                        assessment_weigh: 30,
                        isMarked: "Yes"
                    };
                    marked.push(req);
                }
            }
        }
        Submit_marksStreamingClient submit_marksStreamingClient = check ep->submit_marks();

        foreach AssessmentRequest val in marked {

            check submit_marksStreamingClient->sendAssessmentRequest(val);
        }
        check submit_marksStreamingClient->complete();
        UserResponse? response = check submit_marksStreamingClient->receiveAssessmentResponse();

        if !(response is ()) {
            io:println("Server Response: ", response);
            error? dashboard = Dashboard();

        }

    }
    //return "Course code entered is not allocated to you";
    error? dashboard = Dashboard();

}

function submitAssignment() returns error? {
    AssessmentRequest[] assignments = [];
    string id = io:readln("Enter Learner ID: ");
    int|error user_id = int:fromString(id);

    string Num = io:readln("Enter Many assignments to be submitted: ");
    int|error numOfCourses = int:fromString(Num);

    foreach int i in 1 ... check numOfCourses {
        io:print(i, " ");
        string course_code = io:readln("Enter Course code: ");
        string assignmentWork = io:readln("Type Assignment Content: ");

        AssessmentRequest assign = {
            code: course_code,
            name: id,
            assigmentwork: assignmentWork,
            assessment_mark: 0,
            assessment_weigh: 30,
            isMarked: "No"
        };
        //add if statement, if learner courses are what he is registering for

        assignments.push(assign);

    }
    Submit_assignmentsStreamingClient submit_assignmentsStreamingClient = check ep->submit_assignments();

    foreach AssessmentRequest val in assignments {

        check submit_assignmentsStreamingClient->sendAssessmentRequest(val);
    }
    check submit_assignmentsStreamingClient->complete();
    UserResponse? response = check submit_assignmentsStreamingClient->receiveAssessmentResponse();

    if !(response is ()) {
        io:println("Server Response: ", response);
        error? dashboard = Dashboard();

    }

}

function registerLearner() returns error? {
    UserRequest[] users = [];
    string id = io:readln("Enter Learner ID: ");
    int|error user_id = int:fromString(id);

    string Num = io:readln("Enter How many courses to be registered to: ");
    int|error numOfCourses = int:fromString(Num);

    foreach int i in 1 ... check numOfCourses {
        io:print(i, " ");
        string course_code = io:readln("Enter Course code to Register: ");

        UserRequest user = {
            usertype: "Learner",
            user_id: check user_id,
            firstname: "",
            lastname: "",
            courses: []
        };
        user.courses.push(course_code);
        users.push(user);

    }
    RegisterStreamingClient registerStreamingClient = check ep->register();

    foreach UserRequest val in users {

        check registerStreamingClient->sendUserRequest(val);
    }
    check registerStreamingClient->complete();
    UserResponse? response = check registerStreamingClient->receiveUserResponse();

    if !(response is ()) {
        io:println("Server Response: ", response);
        error? dashboard = Dashboard();

    }

}

function assignCourse() returns error? {

    string code = io:readln("Input Course Code to Assign a lecturer: ");
    string lect_id_string = io:readln("Enter Lecturer ID: ");
    int lect_id = check int:fromString(lect_id_string);

    AssignedCourse tempCourse = {
        course_code: code,
        course_id: 0,
        assessor: {
            usertype: "Assessor",
            user_id: lect_id,
            firstname: "",
            lastname: "",
            courses: []

        }
    };

    CourseResponse courseRes = check ep->assign_course(tempCourse);
    error? dashboard = Dashboard();

}

function createUser() returns error? {

    UserRequest[] users1 = [];
    string user_num = io:readln("Enter How Many User's being created: ");
    int|error user_number = int:fromString(user_num);

    foreach int i in 1 ... check user_number {
        UserRequest tU = {};
        string userTypeString = "";
        string firstname = io:readln("Enter User First Name: ");
        string lastname = io:readln("Enter User Last Name: ");
        string user_id1 = io:readln("Enter User ID: ");
        int|error user_id = int:fromString(user_id1);
        string usertype = io:readln("Learner or Assessor (1=Learner, 2=Assessor): ");
        int|error userTypeID = int:fromString(usertype);
        if (check userTypeID == 1) {
            userTypeString = "Learner";
        } else if (check userTypeID == 2) {
            userTypeString = "Assessor";

        }

        tU.courses = [];
        tU.firstname = firstname;
        tU.lastname = lastname;
        tU.user_id = check user_id;
        tU.usertype = userTypeString;

        users1.push(tU);

    }

    Create_usersStreamingClient create_usersStreamingClient = check ep->create_users();

    foreach var val in users1 {
        check create_usersStreamingClient->sendUserRequest(val);

    }
    check create_usersStreamingClient->complete();
    UserResponse? receiveUser = check create_usersStreamingClient->receiveUserResponse();
    if !(receiveUser is ()) {
        io:println("\nServer Response: ", receiveUser);
        error? dashboard = Dashboard();

    }
}

function createCourse() returns error? {

    Create_courseStreamingClient create_courseStreamingClient = check ep->create_course();

    int[] assignmentWeights = [];
    CourseRequest[] coursesInput = [];

    string ans = "y";

    string num = io:readln("Enter Number of Courses to be created:");
    int|error numOftimes = int:fromString(num);
    foreach int k in 1 ... check numOftimes {

        CourseRequest cr = {};
        string course_id1 = io:readln("Enter the ID of the course:");
        int|error courseID = int:fromString(course_id1);
        string courseCode = io:readln("Enter the course code:");
        string noAssign = io:readln("Enter Number of assignments:");
        int|error noOfAssignments = int:fromString(noAssign);

        foreach int i in 1 ... check noOfAssignments {
            io:println("Assignment ", i);
            string weight = io:readln("Weight: ");
            int|error assignweight = int:fromString(weight);
            assignmentWeights.push(check assignweight);
        }
        cr.NumberOfAssignments = check noOfAssignments;
        cr.weights = assignmentWeights;
        cr.course_id = check courseID;
        cr.course_code = courseCode;
        coursesInput.push(cr);

    }

    future<error?> fut = start Resp(create_courseStreamingClient);

    foreach var item in coursesInput {
        io:println("Creating Course to get Course Code: ", item);
        check create_courseStreamingClient->sendCourseRequest(item);

    }

    check create_courseStreamingClient->complete();

    check wait fut;

}

function Resp(Create_courseStreamingClient create_courseStreamingClient) returns error? {
    string? recieved = check create_courseStreamingClient->receiveString();

    while !(recieved is ()) {
        io:println("Course Code Recieved: ", recieved);
        recieved = check create_courseStreamingClient->receiveString();
        error? dashboard = Dashboard();

    }
}

