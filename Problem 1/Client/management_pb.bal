import ballerina/grpc;
import ballerina/protobuf.types.wrappers;

public isolated client class AssessmentManagementServerClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR_MANAGEMENT, getDescriptorMapManagement());
    }

    isolated remote function assign_course(AssignedCourse|ContextAssignedCourse req) returns CourseResponse|grpc:Error {
        map<string|string[]> headers = {};
        AssignedCourse message;
        if req is ContextAssignedCourse {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("AssessmentManagementServer/assign_course", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <CourseResponse>result;
    }

    isolated remote function assign_courseContext(AssignedCourse|ContextAssignedCourse req) returns ContextCourseResponse|grpc:Error {
        map<string|string[]> headers = {};
        AssignedCourse message;
        if req is ContextAssignedCourse {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("AssessmentManagementServer/assign_course", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <CourseResponse>result, headers: respHeaders};
    }

    isolated remote function request_users(RequestUser|ContextRequestUser req) returns UserRequest|grpc:Error {
        map<string|string[]> headers = {};
        RequestUser message;
        if req is ContextRequestUser {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("AssessmentManagementServer/request_users", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <UserRequest>result;
    }

    isolated remote function request_usersContext(RequestUser|ContextRequestUser req) returns ContextUserRequest|grpc:Error {
        map<string|string[]> headers = {};
        RequestUser message;
        if req is ContextRequestUser {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("AssessmentManagementServer/request_users", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <UserRequest>result, headers: respHeaders};
    }

    isolated remote function create_users() returns Create_usersStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("AssessmentManagementServer/create_users");
        return new Create_usersStreamingClient(sClient);
    }

    isolated remote function submit_assignments() returns Submit_assignmentsStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("AssessmentManagementServer/submit_assignments");
        return new Submit_assignmentsStreamingClient(sClient);
    }

    isolated remote function submit_marks() returns Submit_marksStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("AssessmentManagementServer/submit_marks");
        return new Submit_marksStreamingClient(sClient);
    }

    isolated remote function register() returns RegisterStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("AssessmentManagementServer/register");
        return new RegisterStreamingClient(sClient);
    }

    isolated remote function request_assignments(RequestAssignment|ContextRequestAssignment req) returns stream<AssessmentRequest, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        RequestAssignment message;
        if req is ContextRequestAssignment {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("AssessmentManagementServer/request_assignments", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        AssessmentRequestStream outputStream = new AssessmentRequestStream(result);
        return new stream<AssessmentRequest, grpc:Error?>(outputStream);
    }

    isolated remote function request_assignmentsContext(RequestAssignment|ContextRequestAssignment req) returns ContextAssessmentRequestStream|grpc:Error {
        map<string|string[]> headers = {};
        RequestAssignment message;
        if req is ContextRequestAssignment {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("AssessmentManagementServer/request_assignments", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        AssessmentRequestStream outputStream = new AssessmentRequestStream(result);
        return {content: new stream<AssessmentRequest, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function create_course() returns Create_courseStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeBidirectionalStreaming("AssessmentManagementServer/create_course");
        return new Create_courseStreamingClient(sClient);
    }
}

public client class Create_usersStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendUserRequest(UserRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextUserRequest(ContextUserRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveUserResponse() returns UserResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <UserResponse>payload;
        }
    }

    isolated remote function receiveContextUserResponse() returns ContextUserResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <UserResponse>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class Submit_assignmentsStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendAssessmentRequest(AssessmentRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextAssessmentRequest(ContextAssessmentRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveAssessmentResponse() returns AssessmentResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <AssessmentResponse>payload;
        }
    }

    isolated remote function receiveContextAssessmentResponse() returns ContextAssessmentResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <AssessmentResponse>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class Submit_marksStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendAssessmentRequest(AssessmentRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextAssessmentRequest(ContextAssessmentRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveAssessmentResponse() returns AssessmentResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <AssessmentResponse>payload;
        }
    }

    isolated remote function receiveContextAssessmentResponse() returns ContextAssessmentResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <AssessmentResponse>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class RegisterStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendUserRequest(UserRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextUserRequest(ContextUserRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveUserResponse() returns UserResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <UserResponse>payload;
        }
    }

    isolated remote function receiveContextUserResponse() returns ContextUserResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <UserResponse>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public class AssessmentRequestStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|AssessmentRequest value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|AssessmentRequest value;|} nextRecord = {value: <AssessmentRequest>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public client class Create_courseStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCourseRequest(CourseRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCourseRequest(ContextCourseRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveString() returns string|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return payload.toString();
        }
    }

    isolated remote function receiveContextString() returns wrappers:ContextString|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: payload.toString(), headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class AssessmentManagementServerStringCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendString(string response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextString(wrappers:ContextString response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class AssessmentManagementServerCourseResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendCourseResponse(CourseResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextCourseResponse(ContextCourseResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class AssessmentManagementServerAssessmentRequestCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendAssessmentRequest(AssessmentRequest response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextAssessmentRequest(ContextAssessmentRequest response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class AssessmentManagementServerUserRequestCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendUserRequest(UserRequest response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextUserRequest(ContextUserRequest response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class AssessmentManagementServerAssessmentResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendAssessmentResponse(AssessmentResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextAssessmentResponse(ContextAssessmentResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class AssessmentManagementServerUserResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendUserResponse(UserResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextUserResponse(ContextUserResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextCourseRequestStream record {|
    stream<CourseRequest, error?> content;
    map<string|string[]> headers;
|};

public type ContextUserRequestStream record {|
    stream<UserRequest, error?> content;
    map<string|string[]> headers;
|};

public type ContextAssessmentRequestStream record {|
    stream<AssessmentRequest, error?> content;
    map<string|string[]> headers;
|};

public type ContextUserResponse record {|
    UserResponse content;
    map<string|string[]> headers;
|};

public type ContextAssignedCourse record {|
    AssignedCourse content;
    map<string|string[]> headers;
|};

public type ContextCourseRequest record {|
    CourseRequest content;
    map<string|string[]> headers;
|};

public type ContextRequestAssignment record {|
    RequestAssignment content;
    map<string|string[]> headers;
|};

public type ContextRequestUser record {|
    RequestUser content;
    map<string|string[]> headers;
|};

public type ContextAssessmentResponse record {|
    AssessmentResponse content;
    map<string|string[]> headers;
|};

public type ContextCourseResponse record {|
    CourseResponse content;
    map<string|string[]> headers;
|};

public type ContextUserRequest record {|
    UserRequest content;
    map<string|string[]> headers;
|};

public type ContextAssessmentRequest record {|
    AssessmentRequest content;
    map<string|string[]> headers;
|};

public type UserResponse record {|
    string message = "";
|};

public type AssignedCourse record {|
    string course_code = "";
    int course_id = 0;
    UserRequest assessor = {};
|};

public type CourseRequest record {|
    int course_id = 0;
    string course_code = "";
    string assessorName = "";
    int assessorID = 0;
    int NumberOfAssignments = 0;
    int[] weights = [];
|};

public type RequestAssignment record {|
    string message = "";
    int id = 0;
|};

public type AssessmentResponse record {|
    string message = "";
|};

public type RequestUser record {|
    string usertype = "";
    int user_id = 0;
|};

public type AssessmentRequest record {|
    string code = "";
    string name = "";
    string assigmentwork = "";
    int assessment_weigh = 0;
    int assessment_mark = 0;
    string isMarked = "";
|};

public type CourseResponse record {|
    string message = "";
|};

public type UserRequest record {|
    string usertype = "";
    int user_id = 0;
    string firstname = "";
    string lastname = "";
    string[] courses = [];
|};

const string ROOT_DESCRIPTOR_MANAGEMENT = "0A106D616E6167656D656E742E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F1A1B676F6F676C652F70726F746F6275662F656D7074792E70726F746F223D0A115265717565737441737369676E6D656E7412180A076D65737361676518012001280952076D657373616765120E0A0269641802200128055202696422D1010A114173736573736D656E745265717565737412120A04636F64651801200128095204636F646512120A046E616D6518022001280952046E616D6512240A0D61737369676D656E74776F726B180320012809520D61737369676D656E74776F726B12290A106173736573736D656E745F7765696768180420012803520F6173736573736D656E74576569676812270A0F6173736573736D656E745F6D61726B180520012803520E6173736573736D656E744D61726B121A0A0869734D61726B6564180620012809520869734D61726B6564222E0A124173736573736D656E74526573706F6E736512180A076D65737361676518012001280952076D65737361676522420A0B5265717565737455736572121A0A0875736572747970651801200128095208757365727479706512170A07757365725F6964180220012805520675736572496422DD010A0D436F7572736552657175657374121B0A09636F757273655F69641801200128035208636F757273654964121F0A0B636F757273655F636F6465180220012809520A636F75727365436F646512220A0C6173736573736F724E616D65180320012809520C6173736573736F724E616D65121E0A0A6173736573736F724944180420012803520A6173736573736F72494412300A134E756D6265724F6641737369676E6D656E747318052001280352134E756D6265724F6641737369676E6D656E747312180A0777656967687473180620032803520777656967687473222A0A0E436F75727365526573706F6E736512180A076D65737361676518012001280952076D65737361676522780A0E41737369676E6564436F75727365121F0A0B636F757273655F636F6465180120012809520A636F75727365436F6465121B0A09636F757273655F69641802200128035208636F75727365496412280A086173736573736F7218032001280B320C2E557365725265717565737452086173736573736F722296010A0B5573657252657175657374121A0A0875736572747970651801200128095208757365727479706512170A07757365725F69641802200128055206757365724964121C0A0966697273746E616D65180320012809520966697273746E616D65121A0A086C6173746E616D6518042001280952086C6173746E616D6512180A07636F75727365731805200328095207636F757273657322280A0C55736572526573706F6E736512180A076D65737361676518012001280952076D65737361676532D6030A1A4173736573736D656E744D616E6167656D656E7453657276657212410A0D6372656174655F636F75727365120E2E436F75727365526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C75652801300112310A0D61737369676E5F636F75727365120F2E41737369676E6564436F757273651A0F2E436F75727365526573706F6E7365122B0A0D726571756573745F7573657273120C2E52657175657374557365721A0C2E5573657252657175657374122D0A0C6372656174655F7573657273120C2E55736572526571756573741A0D2E55736572526573706F6E73652801123F0A127375626D69745F61737369676E6D656E747312122E4173736573736D656E74526571756573741A132E4173736573736D656E74526573706F6E7365280112390A0C7375626D69745F6D61726B7312122E4173736573736D656E74526571756573741A132E4173736573736D656E74526573706F6E7365280112290A087265676973746572120C2E55736572526571756573741A0D2E55736572526573706F6E73652801123F0A13726571756573745F61737369676E6D656E747312122E5265717565737441737369676E6D656E741A122E4173736573736D656E74526571756573743001620670726F746F33";

public isolated function getDescriptorMapManagement() returns map<string> {
    return {"google/protobuf/empty.proto": "0A1B676F6F676C652F70726F746F6275662F656D7074792E70726F746F120F676F6F676C652E70726F746F62756622070A05456D70747942540A13636F6D2E676F6F676C652E70726F746F627566420A456D70747950726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "google/protobuf/wrappers.proto": "0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "management.proto": "0A106D616E6167656D656E742E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F1A1B676F6F676C652F70726F746F6275662F656D7074792E70726F746F223D0A115265717565737441737369676E6D656E7412180A076D65737361676518012001280952076D657373616765120E0A0269641802200128055202696422D1010A114173736573736D656E745265717565737412120A04636F64651801200128095204636F646512120A046E616D6518022001280952046E616D6512240A0D61737369676D656E74776F726B180320012809520D61737369676D656E74776F726B12290A106173736573736D656E745F7765696768180420012803520F6173736573736D656E74576569676812270A0F6173736573736D656E745F6D61726B180520012803520E6173736573736D656E744D61726B121A0A0869734D61726B6564180620012809520869734D61726B6564222E0A124173736573736D656E74526573706F6E736512180A076D65737361676518012001280952076D65737361676522420A0B5265717565737455736572121A0A0875736572747970651801200128095208757365727479706512170A07757365725F6964180220012805520675736572496422DD010A0D436F7572736552657175657374121B0A09636F757273655F69641801200128035208636F757273654964121F0A0B636F757273655F636F6465180220012809520A636F75727365436F646512220A0C6173736573736F724E616D65180320012809520C6173736573736F724E616D65121E0A0A6173736573736F724944180420012803520A6173736573736F72494412300A134E756D6265724F6641737369676E6D656E747318052001280352134E756D6265724F6641737369676E6D656E747312180A0777656967687473180620032803520777656967687473222A0A0E436F75727365526573706F6E736512180A076D65737361676518012001280952076D65737361676522780A0E41737369676E6564436F75727365121F0A0B636F757273655F636F6465180120012809520A636F75727365436F6465121B0A09636F757273655F69641802200128035208636F75727365496412280A086173736573736F7218032001280B320C2E557365725265717565737452086173736573736F722296010A0B5573657252657175657374121A0A0875736572747970651801200128095208757365727479706512170A07757365725F69641802200128055206757365724964121C0A0966697273746E616D65180320012809520966697273746E616D65121A0A086C6173746E616D6518042001280952086C6173746E616D6512180A07636F75727365731805200328095207636F757273657322280A0C55736572526573706F6E736512180A076D65737361676518012001280952076D65737361676532D6030A1A4173736573736D656E744D616E6167656D656E7453657276657212410A0D6372656174655F636F75727365120E2E436F75727365526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C75652801300112310A0D61737369676E5F636F75727365120F2E41737369676E6564436F757273651A0F2E436F75727365526573706F6E7365122B0A0D726571756573745F7573657273120C2E52657175657374557365721A0C2E5573657252657175657374122D0A0C6372656174655F7573657273120C2E55736572526571756573741A0D2E55736572526573706F6E73652801123F0A127375626D69745F61737369676E6D656E747312122E4173736573736D656E74526571756573741A132E4173736573736D656E74526573706F6E7365280112390A0C7375626D69745F6D61726B7312122E4173736573736D656E74526571756573741A132E4173736573736D656E74526573706F6E7365280112290A087265676973746572120C2E55736572526571756573741A0D2E55736572526573706F6E73652801123F0A13726571756573745F61737369676E6D656E747312122E5265717565737441737369676E6D656E741A122E4173736573736D656E74526571756573743001620670726F746F33"};
}

