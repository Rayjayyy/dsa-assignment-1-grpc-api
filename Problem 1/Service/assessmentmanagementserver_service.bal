
import ballerina/io;
import ballerina/grpc;

listener grpc:Listener ep = new (9090);

CourseRequest[] courses = [
    {
        course_id: 0,
        course_code: "DPG",
        assessorName: "Suarez",
        assessorID: 2,
        NumberOfAssignments: 3,
        weights: [30, 20]
    },
    {
        course_id: 1,
        course_code: "PRG",
        assessorName: "Suarez",
        assessorID: 2,
        NumberOfAssignments: 3,
        weights: [30, 20, 30]
    }
];
UserRequest[] users = [
    {
        usertype: "Learner",
        firstname: "Neymar",
        lastname: "Jr",
        user_id: 3,
        courses: []
    },
    {
        usertype: "Assessor",
        firstname: "Suarez",
        lastname: "Luis",
        user_id: 2,
        courses: ["DPG", "PRG"]
    },
    {
        usertype: "Administrator",
        firstname: "Messi",
        lastname: "Leo",
        user_id: 4,
        courses: []
    }
];

AssessmentRequest[] assessments = [
    {
        code: "DPG",
        name: "2",
        assigmentwork: "ABC",
        assessment_weigh: 30,
        assessment_mark: 0,
        isMarked: "No"
    },
    {
        code: "PRG",
        name: "3",
        assigmentwork: "ABC",
        assessment_weigh: 30,
        assessment_mark: 0,
        isMarked: "Yes"
    }
];

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR_MANAGEMENT, descMap: getDescriptorMapManagement()}

service "AssessmentManagementServer" on ep {

    remote function assign_course(AssignedCourse value) returns CourseResponse|error|string {

        foreach var item in users {
            if (item.user_id == value.assessor.user_id) {
                item.courses.push(value.course_code);

            }
        }

        CourseResponse resp = {
            message: "Course Assigned Successfully"
        };

        return resp;

    }
    remote function request_users(RequestUser value) returns UserRequest|error|string {

        if (value.usertype == "Assessor") {

            foreach var item in users {
                if (value.user_id == item.user_id) {
                    UserRequest tUser = {};
                    tUser.usertype = "Assessor";
                    tUser.user_id = item.user_id;
                    tUser.firstname = item.firstname;
                    tUser.lastname = item.lastname;
                    tUser.courses = item.courses;

                    return tUser;
                }
            }

        }

        if (value.usertype == "Administrator") {

            foreach var item in users {
                if (value.user_id == item.user_id) {
                    UserRequest tUser = {};
                    tUser.usertype = "Administrator";
                    tUser.user_id = item.user_id;
                    tUser.firstname = item.firstname;
                    tUser.lastname = item.lastname;
                    tUser.courses = item.courses;

                    return tUser;
                }
            }

        }
        if (value.usertype == "Learner") {

            foreach var item in users {
                if (value.user_id == item.user_id) {
                    UserRequest tUser = {};
                    tUser.usertype = "Learner";
                    tUser.user_id = item.user_id;
                    tUser.firstname = item.firstname;
                    tUser.lastname = item.lastname;
                    tUser.courses = item.courses;

                    return tUser;
                }
            }
        }
        UserRequest errUser = {
            usertype: "Error",
            user_id: 0,
            firstname: "Error",
            lastname: "Error",
            courses: []
        };
        return errUser;
    }
    remote function create_users(stream<UserRequest, grpc:Error?> clientStream) returns UserResponse|error|string {

        check clientStream.forEach(function(UserRequest user) {
            io:println("User", user);
            users.push(user);
            io:println("User Array", users);
        });
        io:println(users);
        UserResponse res = {
            message: "User Created"
        };
        return res;
    }
    remote function submit_assignments(stream<AssessmentRequest, grpc:Error?> clientStream) returns AssessmentResponse|error|string {

        check clientStream.forEach(function(AssessmentRequest value) {

            assessments.push(value);
        });
        AssessmentResponse resp = {
            message: "Submitted Successfully"
        };
        return resp;
    }
    remote function submit_marks(stream<AssessmentRequest, grpc:Error?> clientStream) returns AssessmentResponse|error|string {
        check clientStream.forEach(function(AssessmentRequest value) {
            foreach var item in assessments {
                if (item.code == value.code && item.name == value.name && item.isMarked == "No") {
                    item.assessment_mark = value.assessment_mark;
                    item.isMarked = "Yes";
                }
            }
        });
        AssessmentResponse res = {
            message: "Successfully submitted marks"
        };
        return res;

    }
    remote function register(stream<UserRequest, grpc:Error?> clientStream) returns UserResponse|error|string {

        check clientStream.forEach(function(UserRequest user) {

            foreach var item in users {
                if (item.usertype == "Learner") {
                    if (item.user_id == user.user_id) {
                        foreach var userItem in user.courses {
                            item.courses.push(userItem);

                        }
                    }

                }

            }
            io:println(users);
        });

        UserResponse res = {
            message: "Successfully Registered"
        };

        return res;

    }
    remote function request_assignments(RequestAssignment value) returns stream<AssessmentRequest, error?>|error|string {
        AssessmentRequest[] assess = [];
        foreach var item in assessments {
            if (item.code == value.message) {

                if (item.isMarked == "No") {
                    assess.push(item);

                }

            }
            return assess.toStream();

        }

        AssessmentRequest tError = {
            code: "",
            name: "",
            assigmentwork: "",
            assessment_weigh: 0,
            isMarked: "Yes"
        };
        assess.push(tError);
        io:println(assess);
        return assess.toStream();

    }
    remote function create_course(AssessmentManagementServerStringCaller caller, stream<CourseRequest, grpc:Error?> clientStream) returns error? {

        check clientStream.forEach(function(CourseRequest val) {

            courses.push(val);
            io:println(courses);
            checkpanic caller->sendString(val.course_code);

        });
        check caller->complete();
    }
}

